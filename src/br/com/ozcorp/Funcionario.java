package br.com.ozcorp;

/**
 * 
 * @author Melissa Modesto Melo
 *
 */

public class Funcionario {

private String nome;
private  String email;
private int senha; 
private int matricula;
private String sexo;
private String cargo;
private int rg;
private int cpf;
private Sanguineo sanguineo;
private String nivelDeAcesso;
private String salarioBase;
 
public String getNome() {
	return nome;
}

public void setNome(String nome) {
	this.nome = nome;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}


public int getSenha() {
	return senha;
}

public void setSenha(int senha) {
	this.senha = senha;
}

public int getMatricula() {
	return matricula;
}

public void setMatricula(int matricula) {
	this.matricula = matricula;
}

public String getSexo() {
	return sexo;
}

public void setSexo(String sexo) {
	this.sexo = sexo;
}

public String getCargo() {
	return cargo;
}

public void setCargo(String cargo) {
	this.cargo = cargo;
}

public int getRg() {
	return rg;
}

public void setRg(int rg) {
	this.rg = rg;
}

public int getCpf() {
	return cpf;
}

public void setCpf(int cpf) {
	this.cpf = cpf;
}

public String getNivelDeAcesso() {
	return nivelDeAcesso;
}

public void setNivelDeAcesso(String nivelDeAcesso) {
	this.nivelDeAcesso = nivelDeAcesso;
}

public String getSalarioBase() {
	return salarioBase;
}

public void setSalarioBase(String salarioBase) {
	this.salarioBase = salarioBase;
}

public Sanguineo getSanguineo() {
	return sanguineo;
}

public void setSanguineo(Sanguineo sanguineo) {
	this.sanguineo = sanguineo;
}

// Construtor
public Funcionario(String nome, String email, int senha, int matricula, String sexo, String cargo, int rg, int cpf,
		Sanguineo sanguineo, String nivelDeAcesso, String salarioBase) {
	super();
	this.nome = nome;
	this.email = email;
	this.senha = senha;
	this.matricula = matricula;
	this.sexo = sexo;
	this.cargo = cargo;
	this.rg = rg;
	this.cpf = cpf;
	this.sanguineo = sanguineo;
	this.nivelDeAcesso = nivelDeAcesso;
	this.salarioBase = salarioBase;
}
}
 