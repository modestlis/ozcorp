package br.com.ozcorp;

public class FuncionarioTeste {

	public static void main(String[] args) { 
		
		Funcionario ba = new Funcionario("Barbara", "barbara@senai.com.br", 123, 345, "Feminino", "Analista", 123455644, 11234543, Sanguineo.A1, "4", "1.423");
		System.out.println("Dados do funcionario:");
		System.out.println("Nome:" + ba.getNome());
		System.out.println("Email: " + ba.getEmail());
		System.out.println("Senha: " + ba.getSenha());
		System.out.println("Matricula: " + ba.getMatricula());
		System.out.println("Sexo: " + ba.getSexo());
		System.out.println("Cargo:" + ba.getCargo());
		System.out.println("RG: " + ba.getRg());
		System.out.println("CPF:" +ba.getCpf());
		System.out.println("Tipo sanguineo: " + ba.getSanguineo().s);
		System.out.println("Nivel de acesso: " + ba.getNivelDeAcesso());
		System.out.println("Salario base � de:" + ba.getSalarioBase());
		
		System.out.println("**********************************************************************************");
		
		Funcionario bre = new Funcionario("Breno","breno@senai.com.br",456,256,"masculino","Secretario",98567432,5674931,Sanguineo.C1,"1","1.452"); 
		
		System.out.println("Dados do funcionario:");
		System.out.println("Nome:" + bre.getNome());
		System.out.println("Email: " + bre.getEmail());
		System.out.println("Senha: " + bre.getSenha());
		System.out.println("Matricula: " + bre.getMatricula());
		System.out.println("Sexo: " + bre.getSexo());
		System.out.println("Cargo:" + bre.getCargo());
		System.out.println("RG: " + bre.getRg());
		System.out.println("CPF:" +bre.getCpf());
		System.out.println("Tipo sanguineo: " + bre.getSanguineo().s);
		System.out.println("Nivel de acesso: " + bre.getNivelDeAcesso());
		System.out.println("Salario base � de:" + bre.getSalarioBase());
		
		System.out.println("**********************************************************************************");
		
		Funcionario ca = new Funcionario("Carol","carol@senai.com.br",198,657,"feminino","Gerente",67549870,245668097,Sanguineo.A2,"2","4.200"); 
		
		System.out.println("Dados do funcionario:");
		System.out.println("Nome:" + ca.getNome());
		System.out.println("Email: " + ca.getEmail());
		System.out.println("Senha: " + ca.getSenha());
		System.out.println("Matricula: " + ca.getMatricula());
		System.out.println("Sexo: " + ca.getSexo());
		System.out.println("Cargo:" + ca.getCargo());
		System.out.println("RG: " + ca.getRg());
		System.out.println("CPF:" +ca.getCpf());
		System.out.println("Tipo sanguineo: " + ca.getSanguineo().s);
		System.out.println("Nivel de acesso: " + ca.getNivelDeAcesso());
		System.out.println("Salario base � de:" + ca.getSalarioBase());
		
		System.out.println("**********************************************************************************");
		
		Funcionario ro = new Funcionario("Roberto","roberto@senai.com.br",286,195,"Masculino","Diretor",99645386,653783892,Sanguineo.B1,"0","3.200"); 
		
		System.out.println("Dados do funcionario:");
		System.out.println("Nome:" + ro.getNome());
		System.out.println("Email: " + ro.getEmail());
		System.out.println("Senha: " + ro.getSenha());
		System.out.println("Matricula: " + ro.getMatricula());
		System.out.println("Sexo: " + ro.getSexo());
		System.out.println("Cargo:" + ro.getCargo());
		System.out.println("RG: " + ro.getRg());
		System.out.println("CPF:" +ro.getCpf());
		System.out.println("Tipo sanguineo: " + ro.getSanguineo().s);
		System.out.println("Nivel de acesso: " + ro.getNivelDeAcesso());
		System.out.println("Salario base � de:" + ro.getSalarioBase());
		
		System.out.println("**********************************************************************************");
		
		Funcionario ali = new Funcionario("Alicia","alicia@senai.com.br",496,182,"Feminino","Engenheiro",12345679,926748592,Sanguineo.D2,"3","4.900"); 
		
		System.out.println("Dados do funcionario:");
		System.out.println("Nome:" + ali.getNome());
		System.out.println("Email: " + ali.getEmail());
		System.out.println("Senha: " + ali.getSenha());
		System.out.println("Matricula: " + ali.getMatricula());
		System.out.println("Sexo: " + ali.getSexo());
		System.out.println("Cargo:" + ali.getCargo());
		System.out.println("RG: " + ali.getRg());
		System.out.println("CPF:" +ali.getCpf());
		System.out.println("Tipo sanguineo: " + ali.getSanguineo().s);
		System.out.println("Nivel de acesso: " + ali.getNivelDeAcesso());
		System.out.println("Salario base � de:" + ali.getSalarioBase());
		
	}

}
